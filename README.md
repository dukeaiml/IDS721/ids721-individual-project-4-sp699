# IDS-721-Cloud-Computing :computer:

## Individual Project 4 :page_facing_up: 

## :ballot_box_with_check: Requirements
* Rust AWS Lambda function
* Step Functions workflow coordinating Lambdas
* Orchestrate data processing pipeline

## :ballot_box_with_check: Grading Criteria
* __Rust Lambda Functionality__ 30 points
* __Step Functions Workflow__ 30 points
* __Data Processing Pipeline__ 20 points
* __Documentation__ 10 points
* __Demo Video__ 10 points

## :ballot_box_with_check: Main Progress
## Rust AWS Lambda Function
(1) __Rust AWS Lambda Function Overview__ <br/>
: This Individual Project using Rust and AWS Lambda involves reading from a DynamoDB database, which stores data on heights (in cm) and weights (in kg), with each record keyed by user ID. When a user ID is provided, the function fetches the corresponding height and weight from the database to compute the Body Mass Index (BMI) score using the standard formula. Morever, it categorizes each person's obesity status based on their computed BMI score. The output, which includes the BMI score and the obesity category, is then stored in a new table named 'BmiData' within DynamoDB. This setup allows for easy retrieval and viewing of each user's BMI score and obesity status from the database.

(2) __Build and Deploy Rust Lambda Function to AWS Lambda__ <br/>
: After structuring the Lambda function, build it and deploy it to AWS Lambda.
- Build the function
```bash
$ cargo lambda build --release
```
- Deploy the function
```bash
// Deploy the Lambda function specifying the AWS region
$ cargo lambda deploy --region <region>
```
![AWS Lambda](screenshots/AWS Lambda.JPG)

(3) __Attach the policy to Lambda instance__
- Click on the AWS Lambda function.
- Navigate to `Configuration` -> `Permission`.
- Click on the link labeled `Role name`.
- Include permissions for the __GetItem__ and __PutItem__ actions on the `FindBMI` database table, as well as the __PutItem__ action on the `BmiData` database table.

(4) __Test the function__ <br/>
: Test the function to ensure it is working properly. <br/>
![Test Function](screenshots/Test Function.PNG)

- Test json file
```json
{
  "user_id": "1"
}
```
- Result
```json
{
  "BMI": 19.65545082092285,
  "obesity_category": "NormalWeight"
}
```
![DynamoDB Items](screenshots/DynamoDB Items.JPG)

- Confirm the data has been stored in DynamoDB table.

![BmiData Table](screenshots/BmiData Table.JPG)

## Step Functions Workflow <br/>
: AWS Step Functions orchestrate serverless workflows using AWS Lambda functions to execute tasks. By defining a sequence of steps, each invoking a Lambda function, we can manage complex workflows that include conditional logic and error handling. This integration enables seamless automation and data processing without the need to manage infrastructure.

![Step Function](screenshots/Step Function.PNG)
![Step Function-1](screenshots/Step Function-1.PNG)

## Data Processing Pipeline

(1) __Lambda Function Details__
- Describe the specific operations performed by the Rust Lambda function, such as reading user data from DynamoDB, calculating BMI, categorizing obesity status, and then writing these results back to a different DynamoDB table.

_Example 1: Reading and Writing to DynamoDB_
```rust
// Reading user data from DynamoDB
let resp = client.get_item()
    .table_name("FindBMI")
    .key("user_id", AttributeValue::S(id))
    .send()
    .await?;

if let Some(item) = resp.item() {
    let height_cm: f32 = item.get("height_cm").and_then(|v| v.as_n().ok()).and_then(|n| n.parse().ok()).unwrap_or(0.0);
    let weight_kg: f32 = item.get("weight_kg").and_then(|v| v.as_n().ok()).and_then(|n| n.parse().ok()).unwrap_or(0.0);

// Writing results back to a different DynamoDB table
let mut request = client.put_item().table_name("BmiData");
request = request.item("user_id", AttributeValue::S(user_id.to_string()));
request = request.item("bmi", AttributeValue::N(bmi.to_string()));
request = request.item("obesity_category", AttributeValue::S(obesity_category.to_string()));
request.send().await?;
```
- Mention the type of data it processes (e.g., user IDs, heights, and weights), and the logic behind the BMI calculation and categorization.

_Example 2: BMI Calculation and Categorization_
```rust
Copy code
// Calculating BMI
let bmi = calculate_bmi(height_cm, weight_kg);

// Categorizing obesity
let obesity_category = categorize_obesity(bmi);
```

(2) __AWS Lambda Deployment__
- Explain how the Rust code is packaged and deployed to AWS Lambda, possibly mentioning tools like cargo lambda.
```bash
$ cargo lambda deploy --region us-east-2
```

(3) __Step Functions Integration__
- Outline how the Lambda function is orchestrated within AWS Step Functions, describing how it fits into a larger workflow.
```json
{
  "Comment": "A Step Functions state machine that invokes a Lambda function to process BMI",
  "StartAt": "ComputeBMI",
  "States": {
    "ComputeBMI": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:us-east-2:123456789012:function:yourLambdaFunction",
      "Next": "DecidePath"
    },
    "DecidePath": {
      "Type": "Choice",
      "Choices": [
        {
          "Variable": "$.bmi",
          "NumericGreaterThanEquals": 25,
          "Next": "CategorizeObesity"
        }
      ],
      "Default": "NormalWeight"
    },
    "CategorizeObesity": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:us-east-2:123456789012:function:yourOtherLambdaFunction",
      "End": true
    },
    "NormalWeight": {
      "Type": "Pass",
      "End": true
    }
  }
}
```

(4) __Error Handling and Scalability__
- Discuss mechanisms in place for error handling and retries within the AWS Step Functions workflow.
_Example: Error Handling_
```rust
// Error handling in Lambda function
let response = get_bmi(&client, request.user_id.clone()).await.map_err(|e| LambdaError::from(Box::new(e) as Box<dyn std::error::Error + Send + Sync>))?;
```

## :movie_camera: Demo Video
:link: [Demo Video](https://youtu.be/puoku_aynL8)