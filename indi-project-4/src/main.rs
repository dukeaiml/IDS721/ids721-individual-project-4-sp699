use aws_config::load_from_env;
use aws_sdk_dynamodb::{Client, types::AttributeValue};
use lambda_runtime::{Error as LambdaError, LambdaEvent, service_fn};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use simple_logger::SimpleLogger;

#[derive(Deserialize)]
struct Request {
    user_id: String,
}

#[derive(Serialize)]
struct Response {
    bmi: f32,
    obesity_category: String,
}

#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    SimpleLogger::new().with_utc_timestamps().init()?;
    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn handler(event: LambdaEvent<Value>) -> Result<Value, LambdaError> {
    let request: Request = serde_json::from_value(event.payload)
        .map_err(|e| LambdaError::from(Box::new(e) as Box<dyn std::error::Error + Send + Sync>))?;
    
    let config = load_from_env().await;
    let client = Client::new(&config);

    let response = get_bmi(&client, request.user_id.clone()).await?;

    // Save BMI data to DynamoDB
    save_bmi_data(&client, &request.user_id, response.bmi, &response.obesity_category).await?;

    Ok(json!({
        "BMI": response.bmi,
        "obesity_category": response.obesity_category
    }))
}

async fn get_bmi(client: &Client, id: String) -> Result<Response, LambdaError> {
    let table_name = "FindBMI";

    let resp = client.get_item()
        .table_name(table_name)
        .key("user_id", AttributeValue::S(id))
        .send()
        .await?;

    if let Some(item) = resp.item() {
        let height_cm: f32 = item.get("height_cm").and_then(|v| v.as_n().ok()).and_then(|n| n.parse().ok()).unwrap_or(0.0);
        let weight_kg: f32 = item.get("weight_kg").and_then(|v| v.as_n().ok()).and_then(|n| n.parse().ok()).unwrap_or(0.0);
        let bmi = calculate_bmi(height_cm, weight_kg);
        let obesity_category = categorize_obesity(bmi);

        Ok(Response { bmi, obesity_category })
    } else {
        Err(LambdaError::from("No data found for the provided ID"))
    }
}

async fn save_bmi_data(client: &Client, user_id: &str, bmi: f32, obesity_category: &str) -> Result<(), LambdaError> {
    let table_name = "BmiData";

    // Initialize the request builder
    let mut request = client.put_item().table_name(table_name);

    // Build the item to insert into DynamoDB
    request = request.item("user_id", AttributeValue::S(user_id.to_string()));
    request = request.item("bmi", AttributeValue::N(bmi.to_string()));
    request = request.item("obesity_category", AttributeValue::S(obesity_category.to_string()));

    // Send the request and handle the response
    request.send().await
        .map_err(|err| LambdaError::from(Box::new(err) as Box<dyn std::error::Error + Send + Sync>))
        .map(|_| ()) // Convert successful response to unit type, since we don't need the output
}

fn calculate_bmi(height_cm: f32, weight_kg: f32) -> f32 {
    let height_m = height_cm / 100.0;
    weight_kg / (height_m * height_m)
}

fn categorize_obesity(bmi: f32) -> String {
    if bmi < 18.5 {
        "Underweight".to_string()
    } else if bmi < 25.0 {
        "NormalWeight".to_string()
    } else if bmi < 30.0 {
        "Overweight".to_string()
    } else if bmi < 35.0 {
        "ObesityClassI".to_string()
    } else if bmi < 40.0 {
        "ObesityClassII".to_string()
    } else {
        "ObesityClassIII".to_string()
    }
}
